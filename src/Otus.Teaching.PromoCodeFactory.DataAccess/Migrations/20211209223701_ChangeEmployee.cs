﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    public partial class ChangeEmployee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CustomerPreference",
                keyColumn: "Id",
                keyValue: new Guid("a26dd9d8-fcfa-41a0-b235-7edc7b88fdb6"));

            migrationBuilder.AddColumn<int>(
                name: "Age",
                table: "Employees",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.InsertData(
                table: "CustomerPreference",
                columns: new[] { "Id", "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("8777a802-8795-4498-bb9c-d09e78ccd46a"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c") });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CustomerPreference",
                keyColumn: "Id",
                keyValue: new Guid("8777a802-8795-4498-bb9c-d09e78ccd46a"));

            migrationBuilder.DropColumn(
                name: "Age",
                table: "Employees");

            migrationBuilder.InsertData(
                table: "CustomerPreference",
                columns: new[] { "Id", "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("a26dd9d8-fcfa-41a0-b235-7edc7b88fdb6"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c") });
        }
    }
}
