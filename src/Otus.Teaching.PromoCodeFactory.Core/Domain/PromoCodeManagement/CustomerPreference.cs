﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class CustomerPreference: BaseEntity
    {
        public Guid CustomerId { get; set; }
        public virtual Customer Customer { get; set; }// = new List<Customer>();

        public Guid PreferenceId { get; set; }
        public virtual Preference Preference { get; set; }// = new List<Preference>();
    }
}