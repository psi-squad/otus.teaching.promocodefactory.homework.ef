﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить список всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var customersModelList = customers.Select(x =>
                new CustomerShortResponse()
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email
                }).ToList();

            return customersModelList;
        }

        /// <summary>
        /// Получить клиента по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<CustomerResponse> GetCustomerAsync(Guid id)
        {
            //TODO: Добавить получение клиента вместе с выданными ему промомкодами
            var customer = await _customerRepository.GetByIdAsync(id);
            var customerResponse =
                new CustomerResponse()
                {
                    Id = customer.Id,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    Email = customer.Email,
                    PromoCodes = customer.PromoCodes.Select(p => new PromoCodeShortResponse()
                    {
                        Id = p.Id,
                        BeginDate = p.BeginDate.ToShortDateString(),
                        Code = p.Code,
                        EndDate = p.EndDate.ToShortDateString(),
                        ServiceInfo = p.ServiceInfo,
                        PartnerName = p.PartnerName
                    }).ToList(),
                    Preferences = customer.CustomerPreferences.Select(p => new PreferenceResponse() 
                    { 
                         Id = p.Preference.Id,
                         Name = p.Preference.Name
                    }).ToList()
                };

            return customerResponse;

        }

        /// <summary>
        /// Создать клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customerId = Guid.NewGuid();
            IEnumerable<CustomerPreference> customerPreferences = _preferenceRepository.GetAllAsync().Result
                .Where(p => request.PreferenceIds.Contains(p.Id))
                .Select(x => new CustomerPreference() { Id = Guid.NewGuid(), CustomerId = customerId, PreferenceId = x.Id });

            var customer = new Customer()
            {
                Id = customerId,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                CustomerPreferences = customerPreferences.ToList()
            };
            await _customerRepository.CreateAsync(customer);

            return Ok();
        }
        
        /// <summary>
        /// Редактировать клиента
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            //TODO: Обновить данные клиента вместе с его предпочтениями
            IEnumerable<CustomerPreference> customerPreferences = _preferenceRepository.GetAllAsync().Result
                .Where(p => request.PreferenceIds.Contains(p.Id))
                .Select(x => new CustomerPreference() { CustomerId = id, PreferenceId = x.Id });

            var customer = new Customer()
            {
                Id = id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                CustomerPreferences = customerPreferences.ToList()
            };

            await _customerRepository.UpdateAsync(customer);
            return Ok();
        }
        
        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            //TODO: Удаление клиента вместе с выданными ему промокодами
            var customer = _customerRepository.GetByIdAsync(id).Result;
            if (customer != null)
                await _customerRepository.DeleteAsync(customer);
            else
                return NotFound();

            return Ok();
        }
    }
}